import * as React from "react";
import { IStrategy } from "src/contracts/strategy";
import Action from "./actions/Action";
import { Container, Row, Col } from "reactstrap";
import { IStrategyAction } from "src/contracts/strategyAction";
import ActionDropdown from "./ActionDropdown";

interface IStrategyProps {
  strategy: IStrategy;
  onDeleteActionClicked: (actionId: IStrategyAction<any>["id"]) => () => void;
  onCreateActionClicked: (action: IStrategyAction<any>) => void;
}

const Strategy: React.StatelessComponent<IStrategyProps> = ({
  strategy,
  onDeleteActionClicked,
  onCreateActionClicked
}) => (
  <div className="strategy">
    <Container>
      {strategy.actions.map(action => (
        <Row key={action.id}>
          <Col>
            <Action
              action={action}
              onDeleteActionClicked={onDeleteActionClicked(action.id)}
            />
          </Col>
        </Row>
      ))}
      <ActionDropdown onActionCreated={onCreateActionClicked} />
    </Container>
  </div>
);

export default Strategy;
