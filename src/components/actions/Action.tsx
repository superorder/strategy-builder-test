import * as React from "react";
import { IAnyAction } from "src/contracts/strategyAction";
import BuyAction from "./BuyAction";
import SellAction from "./SellAction";
import WaitForRateAction from "./WaitForRateAction";

interface IActionProps {
  action: IAnyAction;
  onDeleteActionClicked: () => void;
}

const Action: React.StatelessComponent<IActionProps> = ({
  action,
  onDeleteActionClicked
}) => {
  switch (action.type) {
    case "WAIT_FOR_RATE":
      return (
        <WaitForRateAction
          action={action}
          onDeleteActionClicked={onDeleteActionClicked}
        />
      );

    case "BUY":
      return (
        <BuyAction
          action={action}
          onDeleteActionClicked={onDeleteActionClicked}
        />
      );

    case "SELL":
      return (
        <SellAction
          action={action}
          onDeleteActionClicked={onDeleteActionClicked}
        />
      );

    default:
      return null;
  }
};

export default Action;
