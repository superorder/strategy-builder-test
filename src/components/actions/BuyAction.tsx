import * as React from "react";
import { IBuy } from "src/contracts/strategyAction";
import { Alert } from "reactstrap";

interface IBuyActionProps {
  action: IBuy;
  onDeleteActionClicked: () => void;
}

const BuyAction: React.StatelessComponent<IBuyActionProps> = ({
  onDeleteActionClicked
}) => {
  return (
    <Alert color="warning" isOpen={true} toggle={onDeleteActionClicked}>
      Buy
    </Alert>
  );
};

export default BuyAction;
