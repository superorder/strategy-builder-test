import * as React from "react";
import { Alert } from "reactstrap";
import { IWaitForRate } from "src/contracts/strategyAction";

interface IWaitForRateActionProps {
  action: IWaitForRate;
  onDeleteActionClicked: () => void;
}

const WaitForRateAction: React.StatelessComponent<IWaitForRateActionProps> = ({
  action,
  onDeleteActionClicked
}) => {
  return (
    <Alert color="info" isOpen={true} toggle={onDeleteActionClicked}>
      Wait for Rate
    </Alert>
  );
};

export default WaitForRateAction;
