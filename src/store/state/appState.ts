import { IStrategy } from "src/contracts/strategy";

export interface IAppState {
  strategy: IStrategy;
}
