import { IStrategy } from "src/contracts/strategy";
import {
  ADD_ACTION,
  DELETE_ACTION,
  IAddAction,
  IDeleteAction
} from "../actions/strategyActions";
import { createInitialStrategyState } from "../state/strategyState";

const strategyReducer = (
  state: IStrategy = createInitialStrategyState(),
  action: IAddAction | IDeleteAction
): IStrategy => {
  switch (action.type) {
    case ADD_ACTION: {
      const { action: strategyAction } = action.payload;

      return {
        ...state,
        actions: [...state.actions, strategyAction]
      };
    }

    case DELETE_ACTION: {
      const { id: actionId } = action.payload;

      return {
        ...state,
        actions: state.actions.filter(a => a.id !== actionId)
      };
    }

    default:
      return state;
  }
};

export default strategyReducer;
